package arrays;

import java.util.*;
import java.util.stream.Collectors;

public class Sorting {
    public static void main(String[] args) {
        List<Test> list = new ArrayList();
        list.add(new Test(3, "Armin", 29, 14.5f, new InnerTest(12, 5)));
        list.add(new Test(2, "Reza", 39, 18.6f, new InnerTest(11, 8)));
        list.add(new Test(4, "Maryam", 32, 16.1f, new InnerTest(18, 3)));
        list.add(new Test(1, "Raha", 41, 12.8f, new InnerTest(10, 6)));

        Collections.sort(list); //sort with comparable
        System.out.println("default: " + list);

        Comparator<Test> byName = Comparator.comparing(Test::getName);
        Collections.sort(list, byName);
        System.out.println("by name: " + list);

        Comparator<Test> byAge = Comparator.comparing(Test::getAge);
        Collections.sort(list, byAge);
        System.out.println("by age: " + list);

        //shorter code(Descending)
        list.sort(Comparator.comparing(Test::getScore).reversed());
        System.out.println("by score: " + list);

        //Above three sort will modify the original list, In case of returning a new list we should use stream
        List<Test> sortedList = list.stream()
                .sorted(Comparator.comparing(Test::getAge))
                .collect(Collectors.toList());
        System.out.println("by age in new Array: " + sortedList);

        System.out.println("Original list still sorted by score(reversed): " + list);

        //sort by nested class field
//        list.sort(Comparator.comparing((test1) -> test1.getInnerTest().getId()));
        list.sort((t1,t2)->t1.getInnerTest().getId().compareTo(t2.getInnerTest().getId()));
        System.out.println("by id of nested inner class: " + list);
    }

}
