package arrays;

import java.util.Objects;

public class Test implements Comparable {
    Integer id;
    String name;
    int age;
    float score;
    InnerTest innerTest;

    public Test(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Test(Integer id, String name, int age, float score, InnerTest innerTest) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.score = score;
        this.innerTest = innerTest;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public InnerTest getInnerTest() {
        return innerTest;
    }

    public void setInnerTest(InnerTest innerTest) {
        this.innerTest = innerTest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return id == test.id && name.equals(test.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", score='" + score + '\'' +
//                ", (Nested id= " + innerTest.getId() + '\'' +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((Test) o).getId());
    }
}
