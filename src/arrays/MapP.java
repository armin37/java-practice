package arrays;

import java.util.HashMap;
import java.util.Map;

public class MapP {
    public static void main(String[] args) {
        Map map = new HashMap();
        map.put(new Test(1, "a"), 1);
        map.put(new Test(2, "b"), 2);
        map.put(new Test(1, "a"), 3);
        System.out.println(map); //1=c 2=b
//        System.out.println(map.put(1, "a")); //null
//        System.out.println(map.put(2, "b")); //null
//        System.out.println(map.put(1, "c")); //a
//        System.out.println(map); //1=c 2=b
//
//        Map map2 = new HashMap();
//        Test t1 = new Test(1, "a");
//        Test t2 = new Test(2, "b");
//        Test t3 = new Test(1, "d");
//        System.out.println(t1.equals(t3)); //false(bad practice, equals and hash must have same results)
//        System.out.println(t1.hashCode()); //same as t3
//        System.out.println(t3.hashCode()); //same as t1
//        map2.put(t1, 1); //adds t1 to map
//        map2.put(t2, 2); //adds t2 to map
//        map2.put(t3, 3); //add t3 to map(map uses equals to check if item exist and hash for indexing, so it adds t3 to the map)
//        System.out.println(map2); //t1 t2 t3
    }
}