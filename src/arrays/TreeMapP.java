package arrays;

import java.util.Map;
import java.util.TreeMap;

public class TreeMapP {
    public static void main(String[] args) {
        Map<Test, Integer> treeMap = new TreeMap();
        treeMap.put(new Test(1, "a"), 1);
        treeMap.put(new Test(3, "b"), 1);
        treeMap.put(new Test(2, "b"), 1);
        System.out.println(treeMap); //1 2 3 (based on Test class implementation of Comparator)
    }
}
