package arrays;

import java.util.Arrays;
import java.util.Comparator;

public class ArraysP {
    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 3};
        int[] unsorted = {5, 9, 4}; // anonymous array(length unspecified)
        int numbers2[] = {1, 2, 3}; // [] can be before or after

        System.out.println(numbers == numbers2);//false

        int[] copyArray = numbers;
        System.out.println(numbers == copyArray);//true

        Arrays.sort(unsorted);
        System.out.println("Sorted Array: " + Arrays.toString(unsorted)); //4 5 9

        char[] unsortedChars = {'g', 'a', 'c'};
        Arrays.sort(unsortedChars);
        System.out.println("Sorted Chars: " + Arrays.toString(unsortedChars)); //a c g

        String[] unsortedString = {"winter", "summer", "autumn", "spring"};
        Arrays.sort(unsortedString);
        System.out.println("Sorted: " + Arrays.toString(unsortedString)); //[autumn, spring, summer, winter]

        String[] numberStrings = {"10", "9", "100"};
        Arrays.sort(numberStrings);
        System.out.println("Not sorting: " + Arrays.toString(numberStrings)); //[10, 100, 9]

        Arrays.sort(numberStrings, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        });
        System.out.println("Numeric string sorted: " + Arrays.toString(numberStrings)); //[9, 10, 100]

        String[] compare = {"ab", "14", "16"};
        String[] compare2 = {"ab", "15", "16"};
        System.out.println(Arrays.compare(compare, compare2)); //false
        System.out.println(Arrays.mismatch(compare, compare2)); //1 (first element arrays are different)


//        Multidimensional Arrays
        int[][] differentSizes = {{1, 4}, {3}, {9, 8, 7}};
    }
}
