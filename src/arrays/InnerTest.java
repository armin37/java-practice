package arrays;

public class InnerTest {
    Integer id;
    int innerScore;

    public InnerTest(Integer id, int innerScore) {
        this.id = id;
        this.innerScore = innerScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInnerScore() {
        return innerScore;
    }

    public void setInnerScore(int innerScore) {
        this.innerScore = innerScore;
    }
}
