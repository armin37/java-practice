package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Convert {
    public static void main(String[] args) {
        List<String> list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");

        String[] stringArray = list.toArray(new String[0]);
        System.out.println(Arrays.toString(stringArray)); //a b c
        list.add("d");
        System.out.println(Arrays.toString(stringArray)); //a b c

        List converted1 = List.of(stringArray);
        List converted2 = Arrays.asList(stringArray);
        stringArray[2] = "e";
        System.out.println("immutable: " + converted1); //a b c
        System.out.println("mutable: " + converted2); //a b e

    }
}
