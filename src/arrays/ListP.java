package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListP {
    public static void main(String[] args) {
        List list1 = new ArrayList();
        list1.add(11);
        list1.add(13);
        list1.set(1, 12);

        System.out.println(list1); //11 12

        list1.remove(1);
        System.out.println(list1); //11

        List list2 = new ArrayList(list1);
        System.out.println(list2); //11

        List unsorted = new ArrayList();
        unsorted.add(5);
        unsorted.add(4);
        unsorted.add(7);
        unsorted.add(6);
        Collections.sort(unsorted);
        System.out.println(unsorted); //4 5 6 7
    }
}
