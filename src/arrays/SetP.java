package arrays;

import java.util.HashSet;
import java.util.Set;

public class SetP {
    public static void main(String[] args) {
        Set set = new HashSet();
        System.out.println(set.add(1));//true
        System.out.println(set.add(2));//true
        System.out.println(set.add(3));//true
        System.out.println(set.add(1));//false
        System.out.println(set);

        Set set2 = new HashSet();
        set2.add(new Test(1, "a"));
        set2.add(new Test(2, "b"));
        set2.add(new Test(1, "a"));
        System.out.println(set2);

        set.remove(new Test(1, "d"));
        set.add(null);
        set.add(null);
        System.out.println(set);
    }
}
