package string;

import java.util.Locale;

public class StringP {
    public static void main(String[] args) {
        String sample = "This is a mutable string from String Pool";

        char ch = sample.charAt(2);
        System.out.println("Character at index 2 is: " + ch); //i

        System.out.println("Length is: " + sample.length()); //41

        int indexOfIs = sample.indexOf("is");
        System.out.println("Index is: " + indexOfIs); // 2(first index is in word 'this')

        indexOfIs = sample.indexOf("is", 4);
        System.out.println("Index is: " + indexOfIs); // 5(first index from index 4)

        int lastIndexOfIs = sample.lastIndexOf("is");
        System.out.println("Index is: " + lastIndexOfIs); // 5

        String sub = sample.substring(2, 4);
        System.out.println("Substring: " + sub); // is

        sample.toUpperCase();
        System.out.println("Sample: " + sample); // because String is mutable nothing will change


        System.out.println("sample" == "SAMPLE"); // false
        System.out.println("sample" == "SAMPLE".toLowerCase()); // false
        System.out.println("sample".equals("SAMPLE")); // false
        System.out.println("sample".equals("SAMPLE".toLowerCase())); // true
        System.out.println("sample".equalsIgnoreCase("SAMPLE")); // true

        System.out.println(sample.startsWith("t")); // false
        System.out.println(sample.startsWith("This")); // true

        sample = sample.replace("This", "It");
        System.out.println(sample);

        System.out.println(sample.contains("It")); //true

        String whiteSpace = "  Another String   ";
        System.out.println(whiteSpace.trim()); //Another String
        System.out.println(whiteSpace.strip()); //Another String(Supports unicode)
        System.out.println(whiteSpace.stripLeading()); //Another String    (removes beginning space)
        System.out.println(whiteSpace.stripTrailing()); //Another String(removes end space)

        StringBuilder sb = new StringBuilder("String");
        sb.append("Builder"); //String Builder is mutable
        sb.insert(6, " "); // insert at certain index
        System.out.println(sb);

        sb.delete(6, 14); // String
        System.out.println(sb);

        sb.deleteCharAt(4); // Strig
        System.out.println(sb);

        StringBuilder s1 = new StringBuilder("1");
        StringBuilder s2 = new StringBuilder("1");
        System.out.println(s1 == s2); //false
        System.out.println(s1.toString().equals(s2.toString())); //true
        StringBuilder s3 = s2.append("5");
        System.out.println(s3 == s2); //true

        String java1 = "java";
        String java2 = "java";
        System.out.println("String pool: " + (java1 == java2)); //true


        String concat = "";
        concat = concat.concat("java");
        System.out.println("String pool: " + (java1 == concat)); //false


        String concat2 = "java";
        concat2 = concat2.concat(""); //if concat length is 0 the same string will be returned
        System.out.println("String pool: " + (java1 == concat2)); //true
    }
}
