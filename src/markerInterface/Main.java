package markerInterface;

public class Main {
    public static void main(String[] args) {
        Shape triangle = new Triangle(5, 4, 6, 7);
        Shape square = new Square(5);

        System.out.println("Triangle area: " + triangle.getArea());
        System.out.println("Triangle circumference: " + triangle.getCircumference());

        System.out.println("Square area: " + square.getArea());
        System.out.println("Square circumference: " + square.getCircumference());

        DAO dao = new DAO();
        System.out.println(dao.deleteShape(triangle));//true
        System.out.println(dao.deleteShape(square));//true

        Course course = new Course();
        System.out.println(dao.delete(course));//true

        NotDeletable notDeletable = new NotDeletable();
        System.out.println(dao.delete(notDeletable));//Not Deletable Entity \n false
    }
}
