package markerInterface;

public class Triangle implements Shape {
    int base;
    int height;
    int side1;
    int side2;

    public Triangle(int base, int height, int side1, int side2) {
        this.base = base;
        this.height = height;
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public Integer getArea() {
        return base * height / 2;
    }

    @Override
    public Integer getCircumference() {
        return side1 + side2 + base;
    }
}
