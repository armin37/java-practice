package markerInterface;

public class DAO {
    public boolean delete(Object o) {
        if (!(o instanceof Deletable)) {
            System.out.println("Not Deletable Entity");
            return false;
        }
        return true;
    }

    public boolean deleteShape(Object o) {
        if (!(o instanceof DeletableShape)) {
            System.out.println("Not Deletable Shape");
            return false;
        }
        return true;
    }
}
