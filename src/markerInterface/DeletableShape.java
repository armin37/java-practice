package markerInterface;

public interface DeletableShape {
    Integer getArea();

    Integer getCircumference();
}
