package markerInterface;

public class Square implements Shape {
    int side;

    public Square(int side) {
        this.side = side;
    }

    @Override
    public Integer getArea() {
        return side * side;
    }

    @Override
    public Integer getCircumference() {
        return side * 4;
    }
}
