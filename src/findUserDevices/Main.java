package findUserDevices;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();

        //add user 1 and devices
        User user1 = new User(3, "Armin");
        user1.getDeviceList().add(new Device("Samsung", "S20"));
        user1.getDeviceList().add(new Device("Samsung", "S21"));
        user1.getDeviceList().add(new Device("Xiaomi", "POCO F3"));
        userList.add(user1);

        //add user 2 and devices
        User user2 = new User(5, "Reza");
        user2.getDeviceList().add(new Device("Apple", "iPhone 11"));
        user2.getDeviceList().add(new Device("Apple", "MacBook Air"));
        userList.add(user2);

        //add user 3 and devices
        User user3 = new User(2, "Maryam");
        user3.getDeviceList().add(new Device("LG", "Chocolate"));
        user3.getDeviceList().add(new Device("Samsung", "A52"));
        user3.getDeviceList().add(new Device("Samsung", "J3"));
        userList.add(user3);

        List<Device> nullDeviceList = findUserDevices(userList, 55);
        System.out.println(nullDeviceList);//null

        List<Device> deviceList = findUserDevices(userList, 5);
        System.out.println(deviceList);// [Device{brand='Apple', model='iPhone 11'}, Device{brand='Apple', model='MacBook Air'}]

        //all match
        boolean allNamesNull = userList.stream().allMatch(u -> u.getName() == null);
        System.out.println("All names are null: " + allNamesNull); //false

        //any match
        boolean anyNameNotNull = userList.stream().anyMatch(u -> u.getName() != null);
        System.out.println("At least one name is Not null: " + anyNameNotNull); //true

        boolean nonNull = userList.stream().noneMatch(u -> u.getName() == null);
        System.out.println("All names are Not null: " + nonNull); //true
    }

    public static List<Device> findUserDevices(List<User> users, int userId) {
        return users.stream()
                .filter(a -> a.getId() == userId)
                .map(a -> a.deviceList)
                .findFirst()
                .orElse(null);
    }
}
