package thread;

public class Anonymous {
    public static void main(String[] args) {
        //Anonymous thread by extending Thread Class
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                setName("Anonymous Inline Thread");
                System.out.println("Inline thread name: " + getName());
                System.out.println("Inline thread name with static function: " + Thread.currentThread().getName());
            }
        };
        thread1.start();

        //Anonymous thread by implementing Runnable
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("Anonymous implementation of Runnable");
                System.out.println("Inline Runnable name: " + Thread.currentThread().getName());
            }
        };
        //Passing Runnable to the constructor of Thread
        Thread thread2 = new Thread(runnable);
        thread2.start();

        //Gets the name of main thead
        System.out.println("main thread name: " + Thread.currentThread().getName());
    }
}
