package thread;

public class ThreadAfterThread {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println(Thread.currentThread().getName() + " Started");
                    Thread.sleep(4000);
                    System.out.println(Thread.currentThread().getName() + " Finished");
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        };

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);

        t1.start();
        try {
            t1.join();
            t2.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
