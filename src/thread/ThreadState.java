package thread;

public class ThreadState {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Thread name: " + Thread.currentThread().getName()); //RUNNABLE!!
                    System.out.println("Thread State 3 | 2: " + Thread.currentThread().getState()); //thread number 1

                    // moving thread to the state timed waiting
                    Thread.sleep(100);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.setName("thread number 1");
        System.out.println("Thread State 1: " + thread.getState()); //New
        thread.start();
        System.out.println("Thread State 2 | 3: " + thread.getState()); //RUNNABLE
        try {
            // moving thread to the state timed waiting
            Thread.sleep(100);
            System.out.println("Thread State 4: " + thread.getState()); //TIMED_WAITING

            Thread.sleep(100);
            System.out.println("Thread State 5: " + thread.getState()); //TERMINATED

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

    }
}
