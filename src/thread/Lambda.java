package thread;

public class Lambda {
    public static void main(String[] args) {
        // Lambda Thread
        new Thread(() -> {
            for (int i = 1; i <= 5; i++) {
                System.out.println("Child Thread: " + i);
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //Thread Example with lambda
        Runnable runnable = () -> {
            System.out.println("Thread2 is running...");
        };
        Thread t2 = new Thread(runnable);
        t2.start();

        System.out.println("Main Thread");

    }
}
