package thread;

public class CallRunnable {
    public static void main(String[] args) {
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                Thread.currentThread().setName("Anonymous implementation of Runnable");
//                System.out.println("Inline Runnable name: " + Thread.currentThread().getName());
//            }
//        };

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= 5; i++) {
                    System.out.println("Runnable Thread: " + i);
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
//                System.out.println("Runnable name: " + Thread.currentThread().getName());
            }
        };
        runnable.run();

        Thread thread = new Thread(runnable);
        thread.start();

        for (int i = 1; i <= 5; i++) {
            System.out.println("Main Thread: " + i + " Thread State: " + thread.getState());
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
