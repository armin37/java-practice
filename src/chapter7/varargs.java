package chapter7;

public class varargs {
    public static void main(String[] args) {
        printVarargs(2);
        printVarargs(2, 4, 6);
//        printVarargs(2, 4, 6, "a");  //Error
    }

    public static void printVarargs(int... args) {
        System.out.println();
        for (int arg : args) {
            System.out.print(arg);
        }
    }
}
