package Chapter6;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class AnimalLambda {
    public static void main(String[] args) {
        // list of animals
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true,
                false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        // Print with custom interface
        print(animals, a -> a.canSwim());

        // Print with builtin Predicate interface
        printWithPredicate(animals, a -> a.canHop());
    }

    private static void print(List<Animal> animals, CheckTrait checker) {
        for (Animal animal : animals) {
            // the general check
            if (checker.test(animal))
                System.out.print(animal + " ");
        }
        System.out.println();
    }

    private static void printWithPredicate(List<Animal> animals, Predicate<Animal> checker) {
        for (Animal animal : animals) {
            // the general check
            if (checker.test(animal))
                System.out.print(animal + " ");
        }
        System.out.println();
    }

    public void caw(final String name) {
        final String volume = "loudly";
        Consumer<String> consumer = s -> System.out.println(name + " says " + volume + " that she is ");
    }
}
