package Chapter6;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaAPIs {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList();
        list.add(14);
        list.add(41);
        list.add(90);
        list.add(74);
        list.add(37);
        list.add(28);

        //find
        List even = list.stream().filter(a -> a % 2 == 0).collect(Collectors.toList());
        System.out.println(even); //14 90 74 28

        // remove all odd numbers
        list.removeIf(num -> num % 2 != 0);
        System.out.println(list); //14 90 74 28

        // If list doesn't have generic type we have to convert Object like below
        /* list.removeIf(num -> {
            return Integer.valueOf(num.toString()) % 2 == 0;
        });*/

        list.sort((a, b) -> a.compareTo(b));
        System.out.println(list); //14 28 74 90

        //because foreach uses consumer and does not return new value, below code does nothing
        list.forEach(a -> a = a * 2);
        System.out.println(list); //14 28 74 90

    }
}
